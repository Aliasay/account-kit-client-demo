<?php
namespace Galiasay\AccountKit;

use Galiasay\AccountKit\Interfaces\ClientInterface;

/**
 * Class Client
 * @package Galiasay\AccountKit
 */
class Client implements ClientInterface
{
    /**
     * @inheritdoc
     */
    public function request($method, $url, $params)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        $data = json_decode(curl_exec($ch), true);

        curl_close($ch);

        return $data;
    }
}