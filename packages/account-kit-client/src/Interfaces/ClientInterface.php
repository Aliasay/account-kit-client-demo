<?php
namespace Galiasay\AccountKit\Interfaces;

/**
 * Interface ClientInterface
 * @package Galiasay\AccountKit\Interfaces
 */
interface ClientInterface
{
    /**
     * @param $method
     * @param $url
     * @param $params
     * @return mixed
     */
    public function request($method, $url, $params);
}