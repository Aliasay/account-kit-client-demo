<?php
namespace Galiasay\AccountKit;

use Galiasay\AccountKit\Exceptions\ResponseException;
use Galiasay\AccountKit\Interfaces\AccountKitInterface;
use Galiasay\AccountKit\Interfaces\ClientInterface;
use Galiasay\AccountKit\Interfaces\ConfigInterface;

/**
 * Class AccountKit
 * @package Galiasay\AccountKit
 */
class AccountKit implements AccountKitInterface
{
    /**
     * @var \Galiasay\AccountKit\Interfaces\ConfigInterface
     */
    protected $config;

    /**
     * @var \Galiasay\AccountKit\Interfaces\ClientInterface
     */
    protected $client;

    /**
     * AccountKit constructor.
     * @param \Galiasay\AccountKit\Interfaces\ConfigInterface $config
     * @param \Galiasay\AccountKit\Interfaces\ClientInterface $client
     */
    public function __construct(ConfigInterface $config, ClientInterface $client)
    {
        $this->config = $config;
        $this->client = $client;
    }

    /**
     * @param $code
     * @return mixed
     * @throws \HttpResponseException
     */
    public function getAccessToken($code)
    {
        $response = $this->client->request('GET', $this->config->getUrlToken(), [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'access_token' => $this->prepareAccessToken(),
        ]);

        if (isset($response['error'])) {
            throw new ResponseException($response['error']['message']);
        }

        return isset($response['access_token']) ? $response['access_token'] : null;
    }

    /**
     * @param $accessToken
     * @return mixed
     */
    public function getData($accessToken)
    {
        $params = [
            'access_token' => $accessToken,
        ];

        if ($this->config->isSecretProof()) {
            $params['appsecret_proof'] = $this->getSecretProof($accessToken);
        }

        $response = $this->client->request('GET', $this->config->getUrlMe(), $params);

        return $response;
    }

    /**
     * @param $accessToken
     * @return mixed
     */
    public function logout($accessToken)
    {
        return $this->client->request('GET', $this->config->getUrlLogout(), [
            'access_token' => $accessToken,
        ]);
    }

    /**
     * @param $accountId
     * @return mixed
     */
    public function invalidateAllTokens($accountId)
    {
        return $this->client->request('GET', $this->config->getUrlInvalidateAllTokens($accountId), [
            'access_token' => $this->config->getAppSecret(),
        ]);
    }

    /**
     * @param $accountId
     * @return mixed
     */
    public function deleteAccount($accountId)
    {
        return $this->client->request('DELETE', $this->config->getUrlInvalidateAllTokens($accountId), [
            'access_token' => $this->prepareAccessToken(),
        ]);
    }

    /**
     * @param int $limit Number of users on the data page.
     * @return mixed
     */
    public function getAccounts($limit = 20)
    {
        return $this->client->request('GET', $this->config->getUrlAccounts(), [
            'access_token' => $this->prepareAccessToken(),
            'limit' => $limit
        ]);
    }

    /**
     * @param $accessToken
     * @return string
     */
    protected function getSecretProof($accessToken)
    {
        return hash_hmac('sha256', $accessToken, $this->config->getAppSecret());
    }

    /**
     * @return string
     */
    protected function prepareAccessToken()
    {
        $appId = $this->config->getAppId();
        $appSecret = $this->config->getAppSecret();
        $appAccessToken = implode('|', ['AA', $appId, $appSecret]);

        return $appAccessToken;
    }
}